package services

import (
	"context"
	"fmt"
	"net/http"
	simPb "widi_sim_manager/grpc/proto"
	"widi_sim_manager/repositories"
)

type Service struct{}

func NewService() repositories.Repositories {
	return Service{}
}

func (s Service) SendSim(ctx context.Context, req *simPb.Request) (*simPb.Response, error) {
	fmt.Println("hello was up")

	return &simPb.Response{
		Meta: &simPb.Meta{
			Code:    http.StatusOK,
			Message: "success Send Email",
			Error:   "",
		},
		Data: &simPb.Request{
			Email: req.Email,
			Name:  req.Name,
		},
	}, nil
}
