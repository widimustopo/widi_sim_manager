package main

import (
	"fmt"
	"net"
	"widi_sim_manager/config"
	"widi_sim_manager/controllers"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

var (
	conf       *config.EnvConfig
	serverGrpc *grpc.Server
)

func init() {
	err := godotenv.Load()
	if err != nil {
		logrus.Fatal(err)
	}

	conf, err = config.NewEnvConfig()
	if err != nil {
		logrus.Fatal(err)
	}
}

func main() {
	serverGrpc = grpc.NewServer(grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
		grpc_ctxtags.StreamServerInterceptor(),
		grpc_recovery.StreamServerInterceptor(),
	)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_ctxtags.UnaryServerInterceptor(),
			grpc_recovery.UnaryServerInterceptor(),
		)),
	)

	controllers.NewServiceSim(serverGrpc)

	listen, err := net.Listen("tcp", fmt.Sprintf(":%v", conf.ServicePort))
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Println("Launching grpc server...")
	logrus.Infof("Server run at %s", conf.ServicePort)
	errService := serverGrpc.Serve(listen)
	if errService != nil {
		logrus.Fatal(errService)
	}

}
