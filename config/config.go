package config

import (
	"os"
)

// EnvConfig hold information of configuration
type EnvConfig struct {
	ServicePort string
}

// NewEnvConfig function to get info in current config file
// @param name string
// TODO : get env for int and boolean
func NewEnvConfig() (*EnvConfig, error) {
	env := &EnvConfig{
		ServicePort: os.Getenv("SERVICE_PORT"),
	}
	return env, nil
}
