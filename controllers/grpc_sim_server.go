package controllers

import (
	"widi_sim_manager/repositories"
	"widi_sim_manager/services"

	simPb "widi_sim_manager/grpc/proto"

	"google.golang.org/grpc"
)

//Here is Controller Repo Grpc And DB connection
type ControlServiceSim struct {
	Repository repositories.Repositories
	simPb.SimManagerServiceServer
}

func NewServiceSim(grpcServer *grpc.Server) *ControlServiceSim {
	repo := services.NewService()
	server := &ControlServiceSim{
		Repository: repo,
	}
	simPb.RegisterSimManagerServiceServer(grpcServer, server)
	return server
}
