package controllers

import (
	"context"
	simPb "widi_sim_manager/grpc/proto"

	"github.com/sirupsen/logrus"
)

func (s ControlServiceSim) SendSim(ctx context.Context, req *simPb.Request) (*simPb.Response, error) {
	response, err := s.Repository.SendSim(ctx, req)
	if err != nil {
		logrus.Error(err.Error())
		return nil, err
	}
	return response, nil
}
