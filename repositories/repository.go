package repositories

import (
	"context"
	simPb "widi_sim_manager/grpc/proto"
)

type Repositories interface {
	SendSim(ctx context.Context, request *simPb.Request) (*simPb.Response, error)
}
